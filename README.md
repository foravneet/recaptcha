## test Google recaptcha v2 check box

reCapcha dev doc - https://developers.google.com/recaptcha/docs/display

POC to try out the flow

1. Click recapcha and submit form -  https://foravneet.gitlab.io/recaptcha/
2. Requestbin for getting value of 'g-recaptcha-response' from submitted form -  http://requestbin.net/r/1i9ue7b1?inspect
3. Verify Response like below after sbstituting value of 'g-recaptcha-response' (dont forget to check for 'error-codes' along with 'success')

$ curl -X POST -d "secret=6Lc_htEUAAAAAE2JHTWckDVHKZqrka2a1g_6hDUD&response=03AOLTBLQXzQTsYeoVHnHlzSyW8eFRh5dE-aV50vLvBVCaSMHd_7AhGBgBa4p7q-cnc-5qQzKzTD6GIJG1bO1Z30vvu-agfHppAuvj-DSTUBDsE3SOO4WdmNrCfVmNOtkR11hMzvPgv1eRzcLh2PbOqHP8VwoiwQkhZwOrvQaRYSqcptK5KG5rjEswzkG45n13Bqqr8RbfcrUvdJL1ofkVmrjCxso-JemO_bCkf3k2oOagL_RULtq4VyibT-1_E5zUCIcOr0nDx7GxtnYzOsjg6rKU1TMxAgJj3EX4Vg2IaOj-XMgWL5h1TDnSaJQ0A4BGnrh5isUEMuLAgJvtaa5jJHlB3m5LTZJbvW4fo5o-okyKzFdkW80QiphInPjadJqN22bSYY7M209i6yHp3DNqKPEonuFAEzlFOGNFk3zKeiRgRw5Rn9JPXqR9W33NfVl5cd-9u7pwl5sIVZ2IQPOAlnVTRcAo71-w0KgKFo5IaoWOlgqN5FHiVWkcy_QgJhjGbClXEDMHP9vkM3zCy0iZM8ApsBTJ_1-wXg" https://www.google.com/recaptcha/api/siteverify

{
  "success": true,
  "challenge_ts": "2020-01-23T20:36:42Z",
  "hostname": "foravneet.gitlab.io"
}


$ curl -X POST -d "secret=6Lc_htEUAAAAAE2JHTWckDVHKZqrka2a1g_6hDUD&response=03AOLTBLQXzQTsYeoVHnHlzSyW8eFRh5dE-aV50vLvBVCaSMHd_7AhGBgBa4p7q-cnc-5qQzKzTD6GIJG1bO1Z30vvu-agfHppAuvj-DSTUBDsE3SOO4WdmNrCfVmNOtkR11hMzvPgv1eRzcLh2PbOqHP8VwoiwQkhZwOrvQaRYSqcptK5KG5rjEswzkG45n13Bqqr8RbfcrUvdJL1ofkVmrjCxso-JemO_bCkf3k2oOagL_RULtq4VyibT-1_E5zUCIcOr0nDx7GxtnYzOsjg6rKU1TMxAgJj3EX4Vg2IaOj-XMgWL5h1TDnSaJQ0A4BGnrh5isUEMuLAgJvtaa5jJHlB3m5LTZJbvW4fo5o-okyKzFdkW80QiphInPjadJqN22bSYY7M209i6yHp3DNqKPEonuFAEzlFOGNFk3zKeiRgRw5Rn9JPXqR9W33NfVl5cd-9u7pwl5sIVZ2IQPOAlnVTRcAo71-w0KgKFo5IaoWOlgqN5FHiVWkcy_QgJhjGbClXEDMHP9vkM3zCy0iZM8ApsBTJ_g" https://www.google.com/recaptcha/api/siteverify

{
  "success": false,
  "error-codes": [
    "invalid-input-response"
  ]
}
